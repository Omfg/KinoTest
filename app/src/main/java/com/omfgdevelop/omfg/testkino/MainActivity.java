package com.omfgdevelop.omfg.testkino;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
Button soonBtn, afishaBtn, myTicketsBtn;
    TextView themeTxt, themeColorTxt, cityTxt, cityChooseTxt, bonusTxt, bonusIntTxt, cardTxt, cardIntTxt, emailTxt, tvemailTxt, exit,
    phonetxt, phoneIntTxt, userTxt,usernameTxt, authTxt,vkUsername;
    ImageView vk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTicketsBtn=(Button)findViewById(R.id.btnMyTitckets);
        myTicketsBtn.setOnClickListener(this);
        vkUsername = (TextView)findViewById(R.id.vkUsername);
        cardIntTxt = (TextView)findViewById(R.id.tvIntCardNomber);
        exit = (Button)findViewById(R.id.exit);
        exit.setOnClickListener(this);
        vk= (ImageView)findViewById(R.id.imageView);
        vk.setOnClickListener(this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnMyTitckets:
                intent =new Intent(this, Tickets.class);
                intent.putExtra("vkUsername",vkUsername.getText().toString());
                intent.putExtra("card",cardIntTxt.getText().toString());
                startActivity(intent);
                break;
            case R.id.imageView:
                vkUsername.setText("Here Is UserName Now");
                break;
            case R.id.exit:
                finish();


        }
    }
}
